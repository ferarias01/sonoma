var args = arguments[0] || {};
/*
 * Evento del manto.
 */
function cerrarMenu() {
    Alloy.Globals.cierraMenu();
}

/*
 * Este metodo limpia las properties al cerrar sesion.
 */
function limpiarProperties() {
    Ti.App.Properties.setBool('login', false);
    Ti.App.Properties.setString('totalCompra', null);
    Ti.App.Properties.setString('idCompra', "0");
    Ti.App.Properties.setString('usuario', null);
    Ti.App.Properties.setString('idUsuario', null);
    Ti.App.Properties.setString('idRotuloDefault', null);
    Ti.App.Properties.setString('contrasena', null);
    Ti.App.Properties.setString('nombreCompleto', null);
    Ti.App.Properties.setString('nombre', null);
    Ti.App.Properties.setString('primerApellido', null);
    Ti.App.Properties.setString('segundoApellido', null);
    Ti.App.Properties.setString('correo', null);
    Ti.App.Properties.setString('telefono', null);
    Alloy.Globals.actualizaIcono();
}

/*
 * Este metodo se encarga de cerrar sesion.
 */
function logout() {
    var idUser = Ti.App.Properties.getString('idUsuario');
    var pTracker = {
        id : idUser,
        category : 'Usuario',
        action : 'Logout'
    };
    limpiarProperties();
    Alloy.Globals.actualizarMenu();
    Alloy.Globals.trackUser(pTracker);
}
function listenerAbreCarrito (respuesta) {
    if (respuesta.length == 0) {
        Alloy.Globals.customDialog(L('msgError'), L('lblCarritoVacio'));
    }else{
        if(OS_IOS){
            Alloy.Globals.navigationWindow.openWindow(Alloy.createController('carrito/carrito').getView());
        } else {
            Alloy.createController('carrito/carrito').getView().open();
        }
    }
};
function abreCarrito() {
    Alloy.Globals.cargarCarrito(listenerAbreCarrito);
}
/*
 * Este metodo es el que abre cada opcion del menu.
 */
function clickMenu() {
    var controller = this.id + '/' + this.id;
    var parametros = '';
    if (this.id == "logout") {
        logout();
    } else if (this.id == "carrito") {
        abreCarrito();
    } else {
        if (this.id == "login") {
            var login = Ti.App.Properties.getBool('login');
            if (!login) {
                controller = 'perfil/iniciarsesion';
                parametros = {
                    module : 'perfil/perfil',
                    param : ''
                };
            }
        }
        Alloy.Globals.nuevaVentana(controller, parametros);
    }
    Alloy.Globals.cierraMenu();
}

/*
 * Este metodo crea dinamicamente las opciones
 * del menu recibiendo como parametro, un arreglo
 * con las opciones.
 */
function crearFilasMenu(arregloModulos) {
    for (var i = 0; i < arregloModulos.length; i++) {
        var filaMenu = Ti.UI.createView({
            id : arregloModulos[i]
        });
        var separador = Ti.UI.createView();
        $.addClass(filaMenu, 'filaMenu');
        $.addClass(separador, 'separatorMenu');
        var lblFilaMenu = Ti.UI.createLabel({
            text : L(arregloModulos[i]),
        });
        $.addClass(lblFilaMenu, 'lblFilaMenu');
        var imgFila = Ti.UI.createImageView({
            image : '/images/menu/' + arregloModulos[i] + '.png'
        });
        $.addClass(imgFila, 'imageMenu');
        filaMenu.addEventListener("singletap", clickMenu);
        filaMenu.add(imgFila);
        filaMenu.add(lblFilaMenu);
        $.menuView.add(filaMenu);
        $.menuView.add(separador);
    }
}

crearFilasMenu(args);
