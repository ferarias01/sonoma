var args = arguments[0] || {};

(function() {
	args.value = 0;
	
	$.textField.width = args.widthText? args.widthText : "70%";
	$.minum.width = args.widthButtons? args.widthButtons : "10%";
	$.plus.width = args.widthButtons? args.widthButtons : "10%";

	$.plus.addEventListener("click", function() {
		var value = $.textField.value;
		value = parseInt(value);
		if (value < 100) {
			value = value + 1;
			$.textField.value = value;
		}

		args.listener(value);
	});

	$.minum.addEventListener("click", function() {
		var value = $.textField.value;
		value = parseInt(value);
		if (value > 0) {
			value = value - 1;
			$.textField.value = value;
		}
		args.listener(value);
	});
})();

