var _obj = arguments[0] || {};

var widgetPicker = {
    maxDate : !_obj.maxDate ? new Date(2020, 11, 31) : _obj.maxDate,
    minDate : !_obj.minDate ? Alloy.Globals.todayDate : _obj.minDate,
    init : function() {
        message = (_obj.message) ? _obj.message : 'Datos no disponibles, favor de intentar nuevamente.';
        if (_obj) {
            if (_obj.text) {
                $.lblText.text = _obj.text + '  ';
                $.lblText.initialValue = _obj.text;
            }
            $.imgDetail.image = _obj.isDate ? '/images/datePicker.png' : $.imgDetail.image = '/images/detail.png';
            if (_obj.enableDelete) {
                $.picker.enableDelete = _obj.enableDelete;
            } else {
                $.picker.enableDelete = false;
            }
            widgetPicker.putDataSubPicker(null);
        };
    },
    cleanLabel : function() {
        $.lblText.text = $.lblText.initialValue;
        $.picker.text = $.lblText.initialValue;
        $.lblText.color = "#acacac";
        if ($.picker.enableDelete) {
            $.lblText.right = '10dp';
            $.imgDetail.right = '7dp';
            $.imgCancel.visible = false;
        }        
        widgetPicker.putDataSubPicker(null);
    },
    showPicker : function() {
        $.picker.removeEventListener('singletap', widgetPicker.showPicker);
        if (_obj.isDate) {
            widgetPicker.isDateState();
        } else if (_obj.data) {
            widgetPicker.isDataState();
        } else {
            alert("No se encontraron los parametros necesarios");
        }
    },
    isDateState : function() {
        var param = {
            type : Ti.UI.PICKER_TYPE_DATE,
            minDate : widgetPicker.minDate,
            maxDate : widgetPicker.maxDate,
            value : Alloy.Globals.todayDate,
            color : "black",
            selectionIndicator : true
        };
        var picker = widgetPicker.getPicker(param);

        if (OS_ANDROID) {
            picker.showDatePickerDialog({
                top : "50dp",
                callback : function(e) {
                    if (!e.cancel) {
                        $.lblText.text = e.value.toLocaleString().split(' ')[2] + " / " + e.value.toLocaleString().split(' ')[1] + " / " + e.value.toLocaleString().split(' ')[3];
                        widgetPicker.putDataSubPicker();
                    }
                }
            });
        } else {
            picker.bottom = "0dp";
            var newWindow = widgetPicker.getWindow();
            newWindow.open({
                animate : true
            });
            picker.addEventListener('change', function(e) {
                $.lblText.text = e.value.toLocaleString().split(' ')[0] + " / " + e.value.toLocaleString().split(' ')[2] + " / " + e.value.toLocaleString().split(' ')[4].replace(",", " ");
                widgetPicker.putDataSubPicker();
            });
            var viewDate = widgetPicker.createViewPickerWindows(newWindow, "datePickerWidget", picker);
            newWindow.addEventListener('click', function(n) {
                newWindow.close();
            });
        }
    },
    isDataState : function() {
        var picker = widgetPicker.getPicker();
        var column = widgetPicker.createColumnData(_obj.data);
        picker.add(column);
        $.picker.add(picker);
        var newWindow = widgetPicker.getWindow();
        /*newWindow.open({
            animated : true
        });*/
        var view = widgetPicker.createViewPickerWindows(newWindow, "PickerWidget", picker);
        if (OS_IOS) {
            newWindow.statusBarStyle = Ti.UI.iPhone.StatusBar.LIGHT_CONTENT;
            view.addEventListener('click', function(n) {
                newWindow.close();
            });
        } else {
            picker.width = "90%";
            var flag = 0;
            /*newWindow.addEventListener('postlayout', function(n) {
                if (flag == 0) {
                    picker.setSelectedRow(0,0,false);
                    flag = 1;
                }
            });*/
        }
        newWindow.addEventListener('click', function(n) {
            newWindow.close();
        });
        picker.addEventListener('change', function(e) {
            $.lblText.color = "#525252";
            $.lblText.text = e.row.title;
            if (OS_ANDROID)
                newWindow.close();
            widgetPicker.putDataSubPicker(e);
            widgetPicker.deleteSuport();
        });
    },
    putDataSubPicker : function(e) {
        if (_obj.subPickerParam) {
            _obj.subPickerParam(e);
        }
    },
    getPicker : function(param) {
        return Ti.UI.createPicker(param);
    },
    getWindow : function() {
        var window = Ti.UI.createWindow();
        if (OS_IOS) {
            window.statusBarStyle = Ti.UI.iPhone.StatusBar.LIGHT_CONTENT;
        }
        $.addClass(window, 'newWindow');
        return window;
    },
    createColumnData : function(array) {
        var column = Ti.UI.createPickerColumn();
        array.forEach(function(rows) {
            var row = Ti.UI.createPickerRow({
                title : rows
            });
            column.addRow(row);
        });
        return column;
    },
    createViewPickerWindows : function(windowPadre, NameClass, picker) {
        var view = Ti.UI.createView();
        $.addClass(view, NameClass);
        view.add(picker);
        windowPadre.add(view);
        return view;
    },
    deleteSuport : function() {
        if ($.picker.enableDelete) {
            $.imgDetail.right = '27dp';
            $.lblText.right = '29dp';
            $.imgCancel.visible = true;
        } else {
            $.imgDetail.right = '5dp';
            $.lblTex.right = '7dp';
            $.imgCancel.cross.visible = false;
        }
    }
};

widgetPicker.init();
