function animateLines(degrees) {
    var t = Ti.UI.create2DMatrix();
    t = t.rotate(degrees);
    var a = Titanium.UI.createAnimation({
        anchorPoint : {
            x : 1,
            y : 1
        },
        transform : t,
        curve : Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT,
        duration : 300
    });
    return a;
    t = null;
    a = null;
}

function isOpenOS(state) {
    if (state) {
        $.lineTop.animate({
            left : 0,
            top : 0,
            duration : 200
        });
        $.lineBottom.animate({
            left : 0,
            bottom : 0,
            duration : 200
        });
        $.lineTop.animate(animateLines(0));
        $.lineBottom.animate(animateLines(0));
        $.lineMiddle.animate({
            width : "30dp",
            left : 0,
            duration : 100
        });
        $.btnMenu.animate({width: '25dp'});
        $.btnMenu.animate(animateLines(0));
    } else {
        $.lineTop.animate({
            left : "20dp",
            top : 2,
            duration : 200
        });
        $.lineBottom.animate({
            left : "20dp",
            bottom : 2,
            duration : 200
        });
        $.lineTop.animate(animateLines(-43));
        $.lineBottom.animate(animateLines(43));
        $.lineMiddle.animate({
            width : "25dp",
            left : 4,
            duration : 200
        });
        $.btnMenu.animate({width: '30dp'});
        $.btnMenu.animate(animateLines(180));
    }
}

function isOpenAndroid(state) {
    if (state) {
        $.lineMiddle.animate({
            width : "30dp",
            right : 0,
            duration : 100
        });
        //$.btnMenu.animate(animateLines(-180));
        $.lineTop.animate(animateLines(0));
        $.lineBottom.animate(animateLines(0));
    } else {
        $.lineMiddle.animate({
            width : "25dp",
            right : "5dp",
            duration : 100
        });
        //$.btnMenu.animate(animateLines(180));
        $.lineTop.animate(animateLines(-39));
        $.lineBottom.animate(animateLines(39));
    }
}
function clickEvent(state){
    if (OS_IOS) {
        isOpenOS(state);
    } else {
        isOpenAndroid(state);
    }
}
$.clickEvent = function(state){
    clickEvent(state);    
};