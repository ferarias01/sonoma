var args = arguments[0] || {};
if (OS_ANDROID) {
	$.meseros.addEventListener('open', function() {
		if (this.activity.actionBar) {
			bar = this.activity.actionBar;
			actionBar = Alloy.Globals.actionBar(bar, 'Meseros');
			actionBar.onHomeIconItemSelected = function() {
				$.meseros.close();
			};
		}
	});
}
$.buttonAtras.addEventListener("click", function() {
	$.meseros.close();
});

$.btnMenu.addEventListener("click", function() {

	Alloy.Globals.nuevaVentana('meseros/menuCategoria', {
		data : ["Snacks", "Bebidas", "Combos"]
	});
});

var dbCon = Alloy.Globals.dataBase.initDB("sonomaDB");
//Alloy.Globals.dataBase.executeQuery(dbCon, "drop TABLE carritoMenu;");
Alloy.Globals.dataBase.executeQuery(dbCon, "CREATE TABLE IF NOT EXISTS carritoMenu(id integer primary key autoincrement, idProd integer not null,quantity integer not null, totalPrice numeric not null);");
//Alloy.Globals.dataBase.executeQuery(dbCon, "insert into carritoMenu (idProd,quantity,totalPrice) values (1111,3,20.10);");
Alloy.Globals.dataBase.closeDB(dbCon);

Ti.App.addEventListener('db_updated', function() {

	for (var i = $.table.data[0].rows.length - 1; i >= 1; i--) {
		$.table.deleteRow(i);
	}

	var dbCon = Alloy.Globals.dataBase.initDB("sonomaDB");
	var rows = Alloy.Globals.dataBase.executeQuery(dbCon, 'select * from carritoMenu;');

	while (rows.isValidRow()) {

		var labelCantidad = Ti.UI.createLabel({
			text : rows.fieldByName('quantity')
		});
		$.addClass(labelCantidad, "rowLabel");
		var viewCantidad = Ti.UI.createView();
		viewCantidad.add(labelCantidad);
		$.addClass(viewCantidad, "viewRowTable");

		var labelNombre = Ti.UI.createLabel({
			text : rows.fieldByName('idProd')
		});
		$.addClass(labelNombre, "rowLabel");
		var viewNombre = Ti.UI.createView();
		viewNombre.add(labelNombre);
		$.addClass(viewNombre, "viewRowTable");
		viewNombre.width = "55%";

		var labelPrecio = Ti.UI.createLabel({
			text : rows.fieldByName('totalPrice')
		});
		$.addClass(labelPrecio, "rowLabel");
		var viewPrecio = Ti.UI.createView();
		viewPrecio.add(labelPrecio);
		$.addClass(viewPrecio, "viewRowTable");

		var button = Ti.UI.createLabel({
			id : rows.fieldByName('id'),
			text : "X"
		});
		$.addClass(button, "button");
		var viewCancel = Ti.UI.createView();
		viewCancel.add(button);
		$.addClass(viewCancel, "viewRowTable");

		button.addEventListener("click", function() {
			var dbCon = Alloy.Globals.dataBase.initDB("sonomaDB");
			Alloy.Globals.dataBase.executeQuery(dbCon, 'delete from carritoMenu where id = '+this.id);
			Alloy.Globals.dataBase.closeDB(dbCon);
			Ti.App.fireEvent('db_updated');
		});

		var viewContenedor = Ti.UI.createView({
			layout : "horizontal"
		});
		$.addClass(viewContenedor, "rowTable");

		viewContenedor.add(viewCantidad);
		viewContenedor.add(viewNombre);
		viewContenedor.add(viewPrecio);
		viewContenedor.add(viewCancel);

		var tableViewRow = Ti.UI.createTableViewRow();
		tableViewRow.add(viewContenedor);

		$.table.appendRow(tableViewRow);

		rows.next();
	}

	Alloy.Globals.dataBase.closeDB(dbCon);
});

Ti.App.addEventListener('closeToMainMenu', function(e) {
	Ti.App.fireEvent('db_updated');
});

Ti.App.fireEvent('db_updated');

