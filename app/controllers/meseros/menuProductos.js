var args = arguments[0] || {};

(function() {

	if (OS_ANDROID) {
		$.menuProductos.addEventListener('open', function() {
			if (this.activity.actionBar) {
				var bar = this.activity.actionBar;
				var actionBar = Alloy.Globals.actionBar(bar, 'Productos');
				actionBar.onHomeIconItemSelected = function() {
					$.menuProductos.close();
				};
			}
		});
	}
	
	$.buttonAtras.addEventListener("click", function() {
		$.menuProductos.close();
	});

	if (args.data) {

		args.data.forEach(function(row,index) {
			var label1 = Ti.UI.createLabel({
				text : row
			});
			$.addClass(label1, "labelProd nomClass");
			var label2 = Ti.UI.createLabel({
				text : row
			});
			$.addClass(label2, "labelProd descClass");
			var label3 = Ti.UI.createLabel({
				text : row
			});
			$.addClass(label3, "labelProd precClass");
			var viewCont = Ti.UI.createView();
			viewCont.add(label1);
			viewCont.add(label2);
			viewCont.add(label3);
			$.addClass(viewCont, "viewInfo");

			var viewRow = Ti.UI.createView();
			$.addClass(viewRow, "viewRow");

			viewRow.add(viewCont);

			var tableViewRow = Ti.UI.createTableViewRow();
			tableViewRow.add(viewRow);
			
			tableViewRow.addEventListener("click",function(){
				Alloy.Globals.nuevaVentana('meseros/menuVistaProducto',{
					idProducto: index,
					name: row,	
					precio: index*20				 
				});
			});

			$.table.appendRow(tableViewRow);
		});

	}
	
	$.butonTerminar.addEventListener("click", function() {
		Ti.App.fireEvent('closeToMainMenu');
	});

	
	Ti.App.addEventListener('closeToMainMenu', function(e) {
		$.menuProductos.close();
	});

})();
