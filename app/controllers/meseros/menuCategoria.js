var args = arguments[0] || {};

(function() {

	if (OS_ANDROID) {
		$.menuCategoria.addEventListener('open', function() {
			if (this.activity.actionBar) {
				var bar = this.activity.actionBar;
				var actionBar = Alloy.Globals.actionBar(bar, 'Categorias');
				actionBar.onHomeIconItemSelected = function() {
					$.menuCategoria.close();
				};
			}
		});
	}
	$.buttonAtras.addEventListener("click", function() {
		$.menuCategoria.close();
	});

	if (args.data) {

		args.data.forEach(function(row) {
			var label1 = Ti.UI.createLabel({
				text : row
			});
			$.addClass(label1, "rowLabel");
			var view1 = Ti.UI.createView();
			view1.add(label1);
			$.addClass(view1, "viewRowTableLeft");

			var label2 = Ti.UI.createLabel({
				text : ">"
			});
			$.addClass(label2, "rowLabel");
			var view2 = Ti.UI.createView();
			view2.add(label2);
			$.addClass(view2, "viewRowTableRight");

			var viewContenedor = Ti.UI.createView({
				layout : "horizontal"
			});

			viewContenedor.add(view1);
			viewContenedor.add(view2);

			var tableViewRow = Ti.UI.createTableViewRow();
			tableViewRow.add(viewContenedor);

			tableViewRow.addEventListener("click", function() {
				Alloy.Globals.nuevaVentana('meseros/menuProductos', {
					data : ["prod 1", "prod 2", "prod 3"]
				});
			});

			$.table.appendRow(tableViewRow);
		});

	}
	
	$.butonTerminar.addEventListener("click", function() {
		Ti.App.fireEvent('closeToMainMenu');
	});

	Ti.App.addEventListener('closeToMainMenu', function(e) {
		$.menuCategoria.close();
	});

})();
