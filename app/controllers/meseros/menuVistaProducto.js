var args = arguments[0] || {};

(function() {

	$.nameProduct.text = args.name ? args.name : "[Nombre de producto]";
	$.price.text = args.precio ? args.precio : 100;
	$.priceTotal.text = 0;
	priceOriginal = parseInt($.price.text);

	$.counter.add(Alloy.createWidget('counter', {
		listener : function(value) {
			var priceTotal = priceOriginal * value;
			$.priceTotal.text = priceTotal;

			var dbCon = Alloy.Globals.dataBase.initDB("sonomaDB");
			
			var row = Alloy.Globals.dataBase.executeQuery(dbCon, "select * from carritoMenu where idProd like '%"+args.name+"%'");
			
            if (row.rowCount == 0) {
                Alloy.Globals.dataBase.executeQuery(dbCon, "insert into carritoMenu (idProd,quantity,totalPrice) values ('"+args.name+"',"+ value +","+priceTotal+");");
            }else{
            	 Alloy.Globals.dataBase.executeQuery(dbCon, "update carritoMenu set quantity ="+value+" , totalPrice = "+priceTotal+" where idProd like '%"+args.name+"%'");
            }
			
			
			Alloy.Globals.dataBase.closeDB(dbCon);
		}
	}).getView());

	if (OS_ANDROID) {
		$.menuVistaProducto.addEventListener('open', function() {
			if (this.activity.actionBar) {
				var bar = this.activity.actionBar;
				var actionBar = Alloy.Globals.actionBar(bar, 'VistaProducto');
				actionBar.onHomeIconItemSelected = function() {
					$.menuVistaProducto.close();
				};
			}
		});
	}

	$.buttonAtras.addEventListener("click", function() {
		$.menuVistaProducto.close();
	});

	$.butonTerminar.addEventListener("click", function() {
		$.menuVistaProducto.close();
		Ti.App.fireEvent('closeToMainMenu');
	});

})();
