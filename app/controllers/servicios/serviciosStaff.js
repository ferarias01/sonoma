var args = arguments[0] || {};

(function() {

	if (OS_ANDROID) {
		$.serviciosStaff.addEventListener('open', function() {
			if (this.activity.actionBar) {
				bar = this.activity.actionBar;
				actionBar = Alloy.Globals.actionBar(bar, args.title? args.title:'DropList');
				actionBar.onHomeIconItemSelected = function() {
					$.serviciosStaff.close();
				};
			}
		});
	}
	$.buttonAtras.addEventListener("click", function() {
		$.serviciosStaff.close();
	});

	if (args.data) {

		args.data.forEach(function(row) {
			var label1 = Ti.UI.createLabel({
				text : row
			});
			$.addClass(label1, "rowLabel");
			var view1 = Ti.UI.createView();
			view1.add(label1);
			$.addClass(view1, "viewRowTableLeft");

			var label2 = Ti.UI.createLabel({
				text : ">"
			});
			$.addClass(label2, "rowLabel");
			var view2 = Ti.UI.createView();
			view2.add(label2);
			$.addClass(view2, "viewRowTableRight");

			var viewContenedor = Ti.UI.createView({
				layout : "horizontal"
			});

			viewContenedor.add(view1);
			viewContenedor.add(view2);

			var tableViewRow = Ti.UI.createTableViewRow();
			tableViewRow.add(viewContenedor);

			tableViewRow.addEventListener("click", function(){
				Alloy.Globals.nuevaVentana('servicios/serviciosReprog', " ");
			});

			$.table.appendRow(tableViewRow);
		});

	}

})();
