var args = arguments[0] || {};

if (OS_ANDROID) {
	$.serviciosReserva.addEventListener('open', function() {
		if (this.activity.actionBar) {
			var bar = this.activity.actionBar;
			var actionBar = Alloy.Globals.actionBar(bar, 'Vista');
			actionBar.onHomeIconItemSelected = function() {
				$.serviciosReserva.close();
			};
		}
	});
}
$.buttonAtras.addEventListener("click", function() {
	$.serviciosReserva.close();
});