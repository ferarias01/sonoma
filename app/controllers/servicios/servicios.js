var args = arguments[0] || {};
var actionBar;
var bar;
if (OS_ANDROID) {
	$.servicios.addEventListener('open', function() {
		if (this.activity.actionBar) {
			bar = this.activity.actionBar;
			actionBar = Alloy.Globals.actionBar(bar, 'Eventos');
			actionBar.onHomeIconItemSelected = function() {
				$.servicios.close();
			};
		}
	});
}
$.buttonAtras.addEventListener("click", function() {
	$.servicios.close();
});

$.scroll.addEventListener('scroll', function(e) {

	var current = e.currentPage;
	if (current == 0) {
		current = "Clases";
	} else {
		current = "Servicios";
	}

	if (OS_ANDROID) {
		actionBar = Alloy.Globals.actionBar(bar, current);
	} else {
		$.title.text = current;
	}

	current = null;
});

var eventClick = function() {
	var params = {
		title : "Clasestaff",
		data : ["Clasestaff 1", "Clasestaff 2", "Clasestaff 3"]
	};
	Alloy.Globals.nuevaVentana('servicios/serviciosStaff', params);
}; 