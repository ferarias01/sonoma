var args = arguments[0] || {};

if (OS_ANDROID) {
	$.eventosCancelaciones.addEventListener('open', function() {
		if (this.activity.actionBar) {
			var bar = this.activity.actionBar;
			var actionBar = Alloy.Globals.actionBar(bar, 'Cancelaciones');
			actionBar.onHomeIconItemSelected = function() {
				$.eventosCancelaciones.close();
			};
		}
	});
}
$.buttonAtras.addEventListener("click", function() {
	$.eventosCancelaciones.close();
});
