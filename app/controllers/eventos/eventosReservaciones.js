var args = arguments[0] || {};

if (OS_ANDROID) {
	$.eventosReservaciones.addEventListener('open', function() {
		if (this.activity.actionBar) {
			var bar = this.activity.actionBar;
			var actionBar = Alloy.Globals.actionBar(bar, 'Eventos Reservacion');
			actionBar.onHomeIconItemSelected = function() {
				$.eventosReservaciones.close();
			};
		}
	});
}
$.buttonAtras.addEventListener("click", function() {
	$.eventosReservaciones.close();
});
$.btnCancela.addEventListener("click", function() {
	Alloy.Globals.nuevaVentana("eventos/eventosCancelaciones", " ");
});

var eventClick = function() {
    Alloy.Globals.nuevaVentana('eventos/eventoReserva');
};
