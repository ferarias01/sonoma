var args = arguments[0] || {};

if (OS_ANDROID) {
	$.eventoReserva.addEventListener('open', function() {
		if (this.activity.actionBar) {
			var bar = this.activity.actionBar;
			var actionBar = Alloy.Globals.actionBar(bar, 'Eventos');
			actionBar.onHomeIconItemSelected = function() {
				$.eventoReserva.close();
			};
		}
	});
}
$.buttonAtras.addEventListener("click", function() {
	$.eventoReserva.close();
});
var valueAnt = 0;
var counter = Alloy.createWidget('counter', {
	widthText: "50dp",
	widthButtons: "30dp",
	listener : function(value) {
			value = value+1;
		if (value > valueAnt) {
			var row = Titanium.UI.createTableViewRow({
				height : "50dp"
			});
			var viewPrincipal = Titanium.UI.createView({
				layout : "horizontal"
			});

			var view1 = Titanium.UI.createView();
			$.addClass(view1, "viewRowTable");
			view1.width = "100%";

			var textField = Titanium.UI.createTextField({
				text : "Dia 1"
			});
			$.addClass(textField, "textField");

			view1.add(textField);

			viewPrincipal.add(view1);

			row.add(viewPrincipal);

			$.table.appendRow(row);
		} else {
			if (value != 0)
				$.table.deleteRow(value);
		}
		valueAnt = value;
		
		if(value > 1){
			$.tableView.visible = true;
		}else{
			$.tableView.visible = false;
		}
	}
}).getView();

$.counterSocios.add(counter);

