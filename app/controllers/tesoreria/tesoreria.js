var args = arguments[0] || {};

if (OS_ANDROID) {
	$.tesoreria.addEventListener('open', function() {
		if (this.activity.actionBar) {
			var bar = this.activity.actionBar;
			var actionBar = Alloy.Globals.actionBar(bar, 'Tesoreria');
			actionBar.onHomeIconItemSelected = function() {
				$.tesoreria.close();
			};
		}
	});
}
$.buttonAtras.addEventListener("click", function() {
	$.tesoreria.close();
});

var openWebViewPDF = function() {
	var webview = Titanium.UI.createWebView({
		url : 'https://www.google.com.mx'
	});
	var window = Titanium.UI.createWindow({
		borderRadius : "10"
	});
	var button = Titanium.UI.createButton({
		top : "0dp",
		right : "0dp",
		title : "X",
		width : "35dp",
		height : "35dp",
		borderRadius : "10"
	});
	window.add(webview);
	window.add(button);
	window.open({
		modal : true
	});
	
	window.addEventListener("open", function() {
		window.activity.actionBar.hide();
	});
	
	button.addEventListener("click",function(){
		window.close();
	});

	

	webview.addEventListener("load", function() {
		
	});
};
