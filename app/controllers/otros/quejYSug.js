var args = arguments[0] || {};

if (OS_ANDROID) {
	$.quejYSug.addEventListener('open', function() {
		if (this.activity.actionBar) {
			bar = this.activity.actionBar;
			actionBar = Alloy.Globals.actionBar(bar, 'Quejas y Sugerencias');
			actionBar.onHomeIconItemSelected = function() {
				$.quejYSug.close();
			};
		}
	});
}
$.buttonAtras.addEventListener("click", function() {
	$.quejYSug.close();
});