var args = arguments[0] || {};

if (OS_ANDROID) {
	$.otros.addEventListener('open', function() {
		if (this.activity.actionBar) {
			bar = this.activity.actionBar;
			actionBar = Alloy.Globals.actionBar(bar, 'Otros');
			actionBar.onHomeIconItemSelected = function() {
				$.otros.close();
			};
		}
	});
}
$.buttonAtras.addEventListener("click", function() {
	$.otros.close();
});

var menuClick = function() {
    Alloy.Globals.nuevaVentana('otros/' + this.id, " ");
};