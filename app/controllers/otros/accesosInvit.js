var args = arguments[0] || {};

if (OS_ANDROID) {
	$.accesoInvit.addEventListener('open', function() {
		if (this.activity.actionBar) {
			bar = this.activity.actionBar;
			actionBar = Alloy.Globals.actionBar(bar, 'Accesos Invitados');
			actionBar.onHomeIconItemSelected = function() {
				$.accesoInvit.close();
			};
		}
	});
}
$.buttonAtras.addEventListener("click", function() {
	$.accesoInvit.close();
});

$.addFamilia.addEventListener("click", function() {
	var ModalWindow = Alloy.createController('otros/modals/modalFamilia').getView();

	var btnCerrar = Ti.UI.createButton({
		title : 'CERRAR',
		top : "5%",
		left : "3%",
		width: "40%",
		height: "85%",
		backgroundColor: "#BDBDBD",
		color: "black",
		font: {
			fontSize: "20dp",
			fontWeight: "bold"
		}
	});

	btnCerrar.addEventListener('click', function() {
		ModalWindow.close();
	});
	
	
	
	var btnAgregar = Ti.UI.createButton({
		title : 'AGREGAR',
		top : "5%",
		right : "3%",
		width: "40%",
		height: "85%",
		backgroundColor: "#BDBDBD",
		color: "black",
		font: {
			fontSize: "20dp",
			fontWeight: "bold"
		}		
	});

	btnAgregar.addEventListener('click', function() {
		
	});

	var viewButtons = Ti.UI.createView({
		width : Titanium.UI.FILL,
		height : "65dp",
		bottom : "0dp",
		backgroundColor : "#C1202E"
	});
	
	viewButtons.add(btnCerrar);
	viewButtons.add(btnAgregar);
	ModalWindow.add(viewButtons);

	ModalWindow.open({
		modal : true
	});
});
