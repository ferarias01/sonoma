(function() {
	var table = Ti.UI.createTableView();	 
	feedMenu(table);
	eventMenu(table);
	$.dashboard.add(table);
	$.index.open();
	
	 Alloy.Globals.nuevaVentana('otros/accesosInvit'," ");
})();

var mStatus = true;
/*
 *  Este metodo hace girar las tres lineas
 *  del menu.
 */
function girarBotonMenu() {
    (OS_IOS) ? $.imgMenu.clickEvent(!mStatus) : $.actionMenu.clickEvent(!mStatus);
}

/*
 * Esta metodo abre el menu.
 */
function abrirMenu() {
    girarBotonMenu();
    $.tablaMenu.animate({
        curve : Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT,
        left : 0,
        duration : 300
    });
    mStatus = !mStatus;
}

/*
 * Este metodo cierra el menu.
 */
function cerrarMenu() {
    girarBotonMenu();
    $.tablaMenu.animate({
        curve : Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT,
        left : "-100%",
        duration : 300
    });
    mStatus = !mStatus;
}

/*
 *  Esta funcion hace publica el cierre del menu.
 */
Alloy.Globals.cierraMenu = function() {
    cerrarMenu();
};

/*
 * Este metodo actualiza las opciones del menu,
 * al iniciar o cerrar sesion.
 */
Alloy.Globals.actualizarMenu = function() {
    if (Ti.App.Properties.getBool('login')) {
        crearFilasMenu(Alloy.CFG.modulesLogout);
    } else {
        crearFilasMenu(Alloy.CFG.modulesLogin);
    }
};
/*
 * Este metodo crea dinamicamente las opciones
 * del menu recibiendo como parametro, un arreglo
 * con las opciones.
 */
function crearFilasMenu(arregloModulos) {
    var menu = Alloy.createWidget('menu', arregloModulos).getView();
    $.tablaMenu.removeAllChildren();
    $.tablaMenu.add(menu);
}

/*
 * Este evento cacha el swipe del menu
 */
function swipe(e) {
    if (e.direction == "right" && mStatus) {
        abrirMenu();
    } else if (e.direction == "left" && !mStatus) {
        cerrarMenu();
    }
}

/*
 *Llena el feed Menu
 */

function feedMenu(tableView){
	var datafeed = ["Noticia 1","Noticia 2","Noticia 3","Noticia 4"];	
	
	var tableRow = Ti.UI.createTableViewRow();
	$.addClass(tableRow,"headerTable");
	var label = Ti.UI.createLabel({
		text: "NEWS FEEDS"
	});
	$.addClass(label,"headerLabel");
	tableRow.add(label);
	tableView.appendRow(tableRow);
	
	Alloy.Globals.descargaListener(datafeed, function(rows){					
		rows.forEach(function(row){
			var tableRow = Ti.UI.createTableViewRow();
			$.addClass(tableRow,"rowTable");
			var label = Ti.UI.createLabel({
				text: "• "+row
			});
			$.addClass(label,"rowLabel");
			tableRow.add(label);
			tableView.appendRow(tableRow);
		});
	});	
	
	datafeed = null;
}

/*
 *Llena el Event menu
 */
function eventMenu(tableView){
	var datafeed = ["Evento 1","Evento 2","Evento 3"];	
	
	var tableRow = Ti.UI.createTableViewRow();
	$.addClass(tableRow,"headerTable");
	var label = Ti.UI.createLabel({
		text: "Eventos proximos"
	});
	$.addClass(label,"headerLabel");
	tableRow.add(label);
	tableView.appendRow(tableRow);
	
	Alloy.Globals.descargaListener(datafeed, function(rows){					
		rows.forEach(function(row){
			var tableRow = Ti.UI.createTableViewRow();
			$.addClass(tableRow,"rowTable");
			var label = Ti.UI.createLabel({
				text: "• "+row
			});
			$.addClass(label,"rowLabel");
			tableRow.add(label);
			tableView.appendRow(tableRow);
		});
	});	
	
	datafeed = null;
}

/*
 * Este es un evento que se ejecuta al abrir el index.
 */
$.index.addEventListener('open', function() {
    if (OS_IOS) {
        $.btnMenuIOS.addEventListener("singletap", function(e) {
            if (mStatus) {
                abrirMenu();
            } else {
                cerrarMenu();
            }
        });
    } else {
        if (this.activity.actionBar) {
            var bar = this.activity.actionBar;
            bar.hide();
            $.index.animate({
                opacity : 1,
                time : 0
            });
        }
        $.btnMenuAndroid.addEventListener("singletap", function(stage) {
            if (mStatus) {
                abrirMenu();
            } else {
                cerrarMenu();
            }
        });
        $.index.activity.addEventListener("resume", function() {
            if (Ti.App.Properties.getBool('login')) {
                // TODO resume Android.
            }
        });
    }
    Alloy.Globals.actualizarMenu();
    Ti.App.addEventListener('resume', function(e) {
        if (Ti.App.Properties.getBool('login')) {
            var usuario = Ti.App.Properties.getString('usuario');
            var contrasena = Ti.App.Properties.getString('contrasena');
            Alloy.Globals.getDataUser(usuario, contrasena, console.log);
        }
    });
});
$.index.addEventListener('android:back', function(e) {
    if (!mStatus) {
        cerrarMenu();
    } else {
        var dialogBack = Ti.UI.createAlertDialog({
            cancel : 1,
            buttonNames : [L('lblSi'), 'No'],
            message : L('confirmaSalir'),
            title : L('msgAviso')
        });
        dialogBack.show();
        dialogBack.addEventListener('click', function(d) {
            if (d.index === d.source.cancel) {
            } else {
                $.index.close();
            }
        });
    }
});