// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

if (OS_IOS) {
    Alloy.Globals.navigationWindow = Ti.UI.iOS.createNavigationWindow();
}
if (OS_ANDROID) {
    Alloy.Globals.actionBar = function(actionBar, title) {
        actionBar.icon = Ti.Android.SHOW_AS_ACTION_IF_ROOM;
        actionBar.logo = '/images/zero.png';
        actionBar.displayHomeAsUp = true;
        actionBar.title = title;
        actionBar.backgroundImage = '/images/background_black.png';
        return actionBar;
    };
}

Alloy.Globals.descargaListener = function(url, listener) {
	/*var xhr = Ti.Network.createHTTPClient({
	 validatesSecureCertificate : false
	 });
	 xhr.onload = function() {
	 listener(this.responseData);
	 };
	 xhr.onerror = function(e) {
	 if (Alloy.Globals.DEBUG) {
	 console.log("Error: " + e);
	 }
	 };
	 xhr.open("GET", url);
	 xhr.send();
	 xhr = null;*/

	listener(url);

}; 

Alloy.Globals.nuevaVentana = function(controller, parametros) {
    if (OS_IOS) {
        Alloy.Globals.navigationWindow.openWindow(Alloy.createController(controller, parametros).getView());
    } else if (OS_ANDROID) {
        Alloy.createController(controller, parametros).getView().open();
    }
};

Alloy.Globals.dataBase = {

    initDB : function(dbName) {
        return Ti.Database.open(dbName);
    },
    executeQuery : function(dbCon, query) {
        return dbCon.execute(query);
    },
    closeDB : function(dbName) {
        return dbName.close();
    }
};

Alloy.Globals.descargaGet = function(url, listener) {
    var xhr = Ti.Network.createHTTPClient({
        validatesSecureCertificate : false
    });
    xhr.onload = function() {
        var respuesta = JSON.parse(this.responseText);
        if (respuesta) {
            listener(respuesta);
        } else {
            Alloy.Globals.customDialog(L('msgError'), L('lblIntente'));
        }
    };
    xhr.onerror = function() {
        if (Alloy.Globals.DEBUG) {
            console.log(this.responseText);
        }
    };
    xhr.open('GET', url);
    if (Titanium.Network.online) {
        xhr.send();
    } else {
        Alloy.Globals.customDialog(L('msgError'), L('lblNoInternet'));
    }
    xhr = null;
};
Alloy.Globals.descargaPost = function(url, parametros, listener) {
    var xhr = Titanium.Network.createHTTPClient({
        validatesSecureCertificate : false
    });
    xhr.open('POST', url);
    xhr.onload = function() {
        var respuesta = JSON.parse(this.responseText);
        if (respuesta) {
            listener(respuesta);
        } else {
            Alloy.Globals.customDialog(L('msgError'), L('lblIntente'));
        }
    };
    xhr.onerror = function() {
        if (Alloy.Globals.DEBUG) {
            console.log(this.responseText);
        }
    };
    xhr.setRequestHeader("Content-Type", "application/json");
    if (Titanium.Network.online) {
        xhr.send(JSON.stringify(parametros));
    } else {
        Alloy.Globals.customDialog(L('msgError'), L('lblNoInternet'));
    }
    xhr = null;
};
Alloy.Globals.getDataUser = function(usuario, contrasena, listener) {
    var parametros = {
        eMail : usuario,
        contrasena : contrasena
    };
    Alloy.Globals.descargaPost(Alloy.CFG.URLS._urlLogin, parametros, listener);
};
